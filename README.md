<a href="#"><img width="100%" height="auto" src="https://i.imgur.com/iXuL1HG.png" height="175px"/></a>

<h1 align="center">Hi <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">, I'm Vincent!</h1>
<h3 align="center">I'm a future software developer</h3>

## 🧑‍💻 About Me

<!-- - 🔭 I’m currently working on **[Password manager cli](https://npmjs.com/package/passwrd)**

-->

- 👯 I’m trying to join the **OpenSource** scene

- 📫 How to reach me <a href="mailto:vincent.nathan.thomas@gmail.com">Mai</a>

## 🚀 Languages and Tools:

<p align="left">
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://img.icons8.com/color/48/000000/javascript.png"/> </a>
    <a href="https://www.w3.org/html/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/html-5.png"/> </a>
    <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/css3.png"/> </a>
    <a href="https://www.python.org" target="_blank"> <img src="https://img.icons8.com/color/48/000000/python.png"/> </a>
    <a style="padding-right:8px;" href="https://nodejs.org" target="_blank"> <img src="https://img.icons8.com/color/48/000000/nodejs.png"/> </a>
    <a style="padding-right:8px;" href="https://www.mysql.com/" target="_blank"> <img src="https://img.icons8.com/fluent/50/000000/mysql-logo.png"/> </a>
    <a href="https://www.mongodb.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="48" height="48"/> </a>
    <a href="https://firebase.google.com/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/firebase.png"/> </a>
    <a href="https://postman.com" target="_blank"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="45" height="45"/> </a>
    <a href="https://git-scm.com/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/git.png"/> </a>
    <a href="https://expressjs.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/express/express-original-wordmark.svg" alt="express" width="40" height="40"/> </a>
    	<a href="https://nestjs.com">
    	<img src="https://docs.nestjs.com/assets/logo-small.svg" width="48"/>
    	</a>

</p>

<br/>
<p align="center">
    <a href="https://github.com/VincentThomas06/github-readme-streak-stats">
        <img title="🔥 Get streak stats for your profile at git.io/streak-stats" alt="Vincent Thomas's streak" src="https://github-readme-streak-stats.herokuapp.com/?user=VincentThomas06&theme=black-ice&hide_border=true&stroke=0000&background=060A0CD0"/>
    </a>
</p>

## 📊 My Github Stats

  <br/>
    <a href="https://github.com/VincentThomas06/github-readme-stats"><img alt="Vincent Thomas's Github Stats" src="https://github-readme-stats.vercel.app/api?username=VincentThomas06&show_icons=true&count_private=true&theme=react&hide_border=true&bg_color=0D1117" /></a>
  <a href="https://github.com/VincentThomas06/github-readme-stats"><img alt="Vincent Thomas's Top Languages" src="https://github-readme-stats.vercel.app/api/top-langs/?username=VincentThomas06&langs_count=8&count_private=true&layout=compact&theme=react&hide_border=true&bg_color=0D1117" /></a>
<br/>
<br/>

<a href="https://github.com/VincentThomas06/github-readme-activity-graph"><img alt="Vincent Thomas's Activity Graph" src="https://activity-graph.herokuapp.com/graph?username=VincentThomas06&bg_color=0D1117&color=5BCDEC&line=5BCDEC&point=FFFFFF&hide_border=true" /></a>

<br/>
<br/>

## Connect with me:

<p align="left">

<a href = "https://twitter.com/subhamraoniar"><img src="https://img.icons8.com/fluent/48/000000/twitter.png"/></a>
<a href = "https://www.instagram.com/subhamraoniar/"><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png"/></a>
<a href = "https://www.youtube.com/channel/UC-NXT1lYAOPa3lrgWXqvuHA"><img src="https://img.icons8.com/color/48/000000/youtube-play.png"/></a>

</p>

## ❤ Views and Followers

<a href="https://github.com/Meghna-DAS/github-profile-views-counter">
    <img src="https://komarev.com/ghpvc/?username=SubhamRaoniar28">
</a>
<a href="https://github.com/VincentThomas06?tab=followers"><img src="https://img.shields.io/github/followers/VincentThomas06?label=Followers&style=social" alt="GitHub Badge"></a>
